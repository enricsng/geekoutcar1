#include "UCMotor.h"

UC_DCMotor motorLeft_1(3, MOTOR34_64KHZ);
UC_DCMotor motorLeft_2(1, MOTOR34_64KHZ);
UC_DCMotor motorRight_1(4, MOTOR34_64KHZ);
UC_DCMotor motorRight_2(2, MOTOR34_64KHZ);

const int LEFT_IR = A0;
const int RIGHT_IR = A1;
const int TRIG_PIN = A2;
const int ECHO_PIN = A3;

const int DEFAULT_SPD = 250; // Default speed
const int BLACK_THRES = 500;

const double MULT = 0.9;

bool is_black(int x) {
    return x <= 500;
}

bool too_close(int x) {
    return x <= 10 || x > 3000;
}

int multiply(int x, double m) {
    return (int)((double)x * m);
}

void setup() {
    Serial.begin(9600);
    pinMode(LEFT_IR, INPUT);
    pinMode(RIGHT_IR, INPUT);
    pinMode(ECHO_PIN, INPUT);
    pinMode(TRIG_PIN, OUTPUT);
}

int readPing() {
    long duration, cm;
    digitalWrite(TRIG_PIN, LOW);
    delayMicroseconds(2);
    digitalWrite(TRIG_PIN, HIGH);
    delayMicroseconds(5);
    digitalWrite(TRIG_PIN, LOW);

    pinMode(ECHO_PIN, INPUT);
    duration = pulseIn(ECHO_PIN, HIGH);

    // convert the time into a distance
    cm = microsecondsToCentimeters(duration);
    return cm;
}

long microsecondsToCentimeters(long microseconds) {
    return microseconds / 29 / 2;
}

/* Movement */

bool check_readings() {
    const int lval = is_black(analogRead(LEFT_IR));
    const int rval = is_black(analogRead(RIGHT_IR));
    return !(lval & rval);
}

bool gradual_left() {
    const int NUM_ITERS = 120; // Arbitrary
    for (int x = 0; x < NUM_ITERS; ++x) {
        left();
        delay(5);
        terminate();
        if (check_readings()) return true;
    }
    return false;
}

bool gradual_right() {
    const int NUM_ITERS = 120; // Arbitrary
    for (int x = 0; x < NUM_ITERS; ++x) {
        right();
        delay(5);
        terminate();
        if (check_readings()) return true;
    }
    return false;
}

void left() {
    motorLeft_1.setSpeed(multiply(DEFAULT_SPD, MULT));
    motorLeft_2.setSpeed(multiply(DEFAULT_SPD, MULT));
    motorRight_1.setSpeed(DEFAULT_SPD);
    motorRight_2.setSpeed(DEFAULT_SPD);

    motorLeft_1.run(BACKWARD);
    motorLeft_2.run(BACKWARD);
    motorRight_1.run(FORWARD);
    motorRight_2.run(FORWARD);
}

void tilt_left() {
    motorLeft_1.setSpeed(0);
    motorLeft_2.setSpeed(0);
    motorRight_1.setSpeed(DEFAULT_SPD);
    motorRight_2.setSpeed(DEFAULT_SPD);

    motorLeft_1.run(FORWARD);
    motorLeft_2.run(FORWARD);
    motorRight_1.run(FORWARD);
    motorRight_2.run(FORWARD);
}

void right() {
    motorLeft_1.setSpeed(multiply(DEFAULT_SPD, MULT));
    motorLeft_2.setSpeed(multiply(DEFAULT_SPD, MULT));
    motorRight_1.setSpeed(DEFAULT_SPD);
    motorRight_2.setSpeed(DEFAULT_SPD);

    motorLeft_1.run(FORWARD);
    motorLeft_2.run(FORWARD);
    motorRight_1.run(BACKWARD);
    motorRight_2.run(BACKWARD);
}

void forward() {
    motorLeft_1.setSpeed(multiply(DEFAULT_SPD, MULT));
    motorLeft_2.setSpeed(multiply(DEFAULT_SPD, MULT));
    motorRight_1.setSpeed(DEFAULT_SPD);
    motorRight_2.setSpeed(DEFAULT_SPD);

    motorLeft_1.run(FORWARD);
    motorLeft_2.run(FORWARD);
    motorRight_1.run(FORWARD);
    motorRight_2.run(FORWARD);
}

void backward() {
    motorLeft_1.setSpeed(multiply(DEFAULT_SPD, MULT));
    motorLeft_2.setSpeed(multiply(DEFAULT_SPD, MULT));
    motorRight_1.setSpeed(DEFAULT_SPD);
    motorRight_2.setSpeed(DEFAULT_SPD);

    motorLeft_1.run(BACKWARD);
    motorLeft_2.run(BACKWARD);
    motorRight_1.run(BACKWARD);
    motorRight_2.run(BACKWARD);
}

void terminate() {
    motorLeft_1.setSpeed(0);
    motorLeft_2.setSpeed(0);
    motorRight_1.setSpeed(0);
    motorRight_2.setSpeed(0);

    motorLeft_1.run(FORWARD);
    motorLeft_2.run(FORWARD);
    motorRight_1.run(FORWARD);
    motorRight_2.run(FORWARD);
}

/* Enums */
const int TRACK = 1;
const int SEARCH = 2;
const int BOX = 3;

int STATE = TRACK;
int MEM = 0; // 1 = left, 2 = right

const int missing_thres = 20; // Threshold
int missing_cnt = 0;

int box_cnt = 0;

void loop() {
    Serial.println(STATE);
    /* Get readings */
    const int lval = is_black(analogRead(LEFT_IR));
    const int rval = is_black(analogRead(RIGHT_IR));
    const int dist = readPing();
    if (STATE == TRACK) {
        /* See if we need to jump state */
        if (too_close(dist) && box_cnt++ < 1) {
            STATE = BOX;
        } else if ((lval & rval) & (missing_cnt >= missing_thres)) { // Both are black
            missing_cnt = 0;
            STATE = SEARCH;
        } else { // Just align

            if (lval & rval) ++missing_cnt;
            else missing_cnt = 0;

            if (lval & !rval) { // Turn right (slightly)
                MEM = 2;
                right();
            } else if (!lval & rval) { // Turn left (slightly)
                MEM = 1;
                left();
            } else if (!lval & !rval) { // Move forward
                forward();
            } else if (MEM == 1) left();
            else if (MEM == 2) right();
        }
    }
    else if (STATE == SEARCH) {
        bool latch = false;
        // Try left
        latch |= gradual_left(); // 90º
        terminate();
        // Try right
        if (!latch) {
            gradual_right();
            latch |= gradual_right();
            terminate();
        }
        // Give up; Just inch forward
        // if (!latch) { // Re-adjust
        //     gradual_left();
        //     forward();
        //     delay(750);
        // }
        STATE = TRACK;
    }
    else if (STATE == BOX) {
        backward(); delay(150);
        tilt_left(); delay(300);
        forward(); delay(1500);
        STATE = TRACK;
    }
    // delay(10);
}
