#CHECKLIST WHEN DEBUGGING
1. [upload error]
	1a. check port
	1b. check board type
	1c. check if ethernet shield is mounted. If so, dismount it.
	1d. hardware check: is arduino powered on? 
2. [compile error]
	2a. read through error messages carefully and rectify accordingly
3. [no AP is found]
	3a. is blue light on? If not, battery is running low, probably. Replace/charge them!
4. [on phone app, connection failed]
	4a. phone is not on car's network, hook it up!

