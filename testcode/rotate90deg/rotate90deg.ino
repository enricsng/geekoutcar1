#include "UCMotor.h"

#define MAX_SPEED 200

//creaing UC_DCMotor
UC_DCMotor leftMotor1(3, MOTOR34_64KHZ);
UC_DCMotor rightMotor1(4, MOTOR34_64KHZ);
UC_DCMotor leftMotor2(1, MOTOR34_64KHZ);
UC_DCMotor rightMotor2(2, MOTOR34_64KHZ);

void setup()
{
  leftMotor1.setSpeed(MAX_SPEED);
  rightMotor1.setSpeed(MAX_SPEED);
  leftMotor2.setSpeed(MAX_SPEED);
  rightMotor2.setSpeed(MAX_SPEED);
  stop();
}

void loop() {

  delay(5000);
  long initialTime = 0;
  
  while(millis()<500){
    turnRight();
    }
    
}



void moveForward()
{
  leftMotor1.run(FORWARD);
  rightMotor1.run(FORWARD);
  leftMotor2.run(FORWARD);
  rightMotor2.run(FORWARD);
}

void moveBackward()
{
  leftMotor1.run(BACKWARD);
  rightMotor1.run(BACKWARD);
  leftMotor2.run(BACKWARD);
  rightMotor2.run(BACKWARD);
}

void turnRight()
{
  leftMotor1.run(FORWARD);
  rightMotor1.run(BACKWARD);
  leftMotor2.run(FORWARD);
  rightMotor2.run(BACKWARD);
}


void turnLeft()
{
  leftMotor1.run(BACKWARD);
  rightMotor1.run(FORWARD);
  leftMotor2.run(BACKWARD);
  rightMotor2.run(FORWARD);
}

void stop()
{
  leftMotor1.run(5); rightMotor1.run(5);
  leftMotor2.run(5); rightMotor2.run(5);
}
