#define TRIG_PIN A2
#define ECHO_PIN A3

int obstacleAlert = ; //1. LED pin assignment
int TimerConfigVal = 100;

bool detected_flag = ;//2. boolean varaible, used for Flag
//FALSE when not detected; TRUE when detected.
//how do you want to define 'detected?'
//you may need to make use of Serial monitor to decide on the threshold


void setup() {
  pinMode(ECHO_PIN, INPUT); //Set the connection pin output mode Echo pin
  pinMode(TRIG_PIN, OUTPUT);//Set the connection pin output mode trog pin
  //3. pin mode for LED
}

void loop() {
  int cm = readPing(); //4. readPing() returns distance away from an obtacle
  //LED starts blinking when obstacle is wihtin the specified threshold
  //Once it starts blinking, blinking frequency changes according to
  //distance i.e. when nearer, LED blinks faster

  int timerFreq = cm + TimerConfigVal;
  delay(timerFreq);
}

//You do not need to modify the following functions
//feel free to make use of readPing() and microsecondsToCentimeters(long microseconds)
int readPing() {
    long duration, cm;
    digitalWrite(TRIG_PIN, LOW);
    delayMicroseconds(2);
    digitalWrite(TRIG_PIN, HIGH);
    delayMicroseconds(5);
    digitalWrite(TRIG_PIN, LOW);

    pinMode(ECHO_PIN, INPUT);
    duration = pulseIn(ECHO_PIN, HIGH);

    // convert the time into a distance
    cm = microsecondsToCentimeters(duration);
    return cm;
}

long microsecondsToCentimeters(long microseconds) {
    return microseconds / 29 / 2;
}
