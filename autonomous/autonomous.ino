#include "UCMotor.h"

UC_DCMotor motorLeft_1(3, MOTOR34_64KHZ);
UC_DCMotor motorLeft_2(1, MOTOR34_64KHZ);
UC_DCMotor motorRight_1(4, MOTOR34_64KHZ);
UC_DCMotor motorRight_2(2, MOTOR34_64KHZ);

const int LEFT_IR = A0;
const int RIGHT_IR = A1;

const int BLACK_THRES = 500; // More than 500 = white

int memory; // 0 = not set, 1 = left, 2 = right
const double mult = 0.9; // Make left slower
const int default_speed = 250;
const int reverse_speed = 50;

bool is_black(int x) {
  return x <= 500;
}

int multiply(int x, double m) {
  return (int)((double)x * m);
}

void forward(){
  const int l_speed = multiply(default_speed, mult);
  const int r_speed = default_speed;
  motorLeft_1.setSpeed(l_speed);
  motorLeft_2.setSpeed(l_speed);
  motorRight_1.setSpeed(r_speed);
  motorRight_2.setSpeed(r_speed);

  motorLeft_1.run(FORWARD);
  motorLeft_2.run(FORWARD);
  motorRight_1.run(FORWARD);
  motorRight_2.run(FORWARD);
}

void left() {
  memory = 1;
  const int r_speed = default_speed;

  motorLeft_1.setSpeed(reverse_speed);
  motorLeft_2.setSpeed(reverse_speed);
  motorRight_1.setSpeed(r_speed);
  motorRight_2.setSpeed(r_speed);

  motorLeft_1.run(BACKWARD);
  motorLeft_2.run(BACKWARD);
  motorRight_1.run(FORWARD);
  motorRight_2.run(FORWARD);
}

void right() {
  memory = 2;
  const int l_speed = multiply(default_speed, mult);

  motorLeft_1.setSpeed(l_speed);
  motorLeft_2.setSpeed(l_speed);
  motorRight_1.setSpeed(reverse_speed);
  motorRight_2.setSpeed(reverse_speed);

  motorLeft_1.run(FORWARD);
  motorLeft_2.run(FORWARD);
  motorRight_1.run(BACKWARD);
  motorRight_2.run(BACKWARD);
}

void terminate() {
  motorLeft_1.setSpeed(0);
  motorLeft_2.setSpeed(0);
  motorRight_1.setSpeed(0);
  motorRight_2.setSpeed(0);
}

void setup() {
  Serial.begin(9600);
  pinMode(LEFT_IR, INPUT);
  pinMode(RIGHT_IR, INPUT);
}

void loop()
{
  int lval = analogRead(LEFT_IR);
  int rval = analogRead(RIGHT_IR);

  if (!is_black(rval)) {
    if (!is_black(lval)) {
      forward();
    } else {
      right();
    }
  } else {
    if (!is_black(lval)) {
      left();
    } else {
      if (memory == 1) left();
      else if (memory == 2) right();
    }
  }
  delay(10);
}
