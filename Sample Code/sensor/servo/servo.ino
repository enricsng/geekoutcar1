/* Sweep
 by BARRAGAN <http://barraganstudio.com>
 This example code is in the public domain.

 modified 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Sweep
*/

/*Objective
Control one/two servo(s)
*/
//1. Attach servo library Servo.h

Servo #input ;  //2. create servo object to control a servo


//3. variable to store the servo position 

void setup() {
  //4. pin assignment 
  #input.attach();  //5. attaches the servo on pin 9 to the servo object
  //6. Serial monitor!
}

void loop() {
  //7. let's write command on serial monitor to ..
  #input.write()//8. tell servo to go to position
}

