/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */

 /*
  for loop example
  */
int led = 13;//1. assign pin

//the setup routine runs once when you press reset:
void setup() {                
  //2. initialize the digital pin as an output.
  pinMode(led,OUTPUT);    

}

//the loop routine runs over and over again forever:
void loop() {
  int count =5;
  int timer = 300; //delay timer
  for (int iterator=0; iterator < count; iterator++) {
    // turn led on:
    //3a. turn the LED on (HIGH is the voltage level)
    digitalWrite(led, HIGH);
    delay(timer);
    Serial.println("on");
    // turn led off:
    //3b. turn the LED off (LOW is the voltage level)
    digitalWrite(led, LOW);
    delay(timer);
  }  
  delay(1000);
}
