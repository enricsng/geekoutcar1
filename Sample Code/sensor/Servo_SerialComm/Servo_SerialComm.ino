/* Sweep
 by BARRAGAN <http://barraganstudio.com>
 This example code is in the public domain.

 modified 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Sweep
*/

//https://gist.github.com/mitchtech/3029316

#include <Servo.h>
char servoMove = 'm'; //sweep through ~0 to ~180
char servoStop = 's';
char serialChar;

// 1. create servo object to control a servo


int // 2. variable to store the servo position
int prev_pos = 0;

void setup() {
  #input.attach(#input);  //3.attaches the servo on pin 9 to the servo object
  Serial.begin(#input); //4.set up a serial connection for 9600 bps
  Serial.println("to begin send command. m - to sweep/move s - to stop");
  int current_pos = myservo.read(); 
  prev_pos = current_pos;
  Serial.print("current position is at :");
  Serial.println(prev_pos);
}

void loop() {
  if(Serial.available()){ //wait for a character on serial port
    serialChar = Serial.read(); //reading from the serial port, store on serialChar
    Serial.print("Received command is: ");
    Serial.println(serialChar);
    //5. let servo sweeps through with command 'm'
    //by making use of functions CW() and ACW()
  }  
}

void CW(){
  for (pos = 180; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    Serial.print("CW moving to "); 
    Serial.println(pos);    
    #input.write(pos);              // tell servo to go to position in variable 'pos'
    delay(100);                       // waits 15ms for the servo to reach the position
    prev_pos = pos;
  }  
}

void ACW(){
  for (pos = prev_pos; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    Serial.print("ACW moving to "); 
    Serial.println(pos);      
    #input.write(pos);              // tell servo to go to position in variable 'pos'
    delay(100);                       // waits 15ms for the servo to reach the position
    prev_pos = pos;
  }  
}

