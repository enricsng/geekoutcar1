/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
int //1. assign pin

//the setup routine runs once when you press reset:
void setup() {                
  //2. initialize the digital pin as an output.
  pinMode(13, OUTPUT);  
     
}

void loop() {
  digitalWrite(13,HIGH);   
  delay(300);
  digitalWrite(13,LOW);
  delay(300);
  
  //3. turn the LED on (HIGH is the voltage level)
  //4. Make LED blink 
}
