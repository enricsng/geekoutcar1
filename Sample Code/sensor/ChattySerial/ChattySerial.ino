void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);//1. comm with pc via usb to serial monitor
  //serial.begin initializes built-in UART USB module
  //through this port, you can receive and send commands
  //hardware port - digital pin 0, 1 (UART USB module)
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  //2. pin mode for LED
  pinMode(13, OUTPUT);
  
}

void loop() { // run over and over
  if (Serial.available()) {
    //available tells you how many bytes are in the received buffer
    char input = Serial.read();    
    Serial.print("you typed ");
    Serial.println(input);
    
    //4. turns on LED IF, serial command is 'o'
    if ((char)input == 'o') {
      digitalWrite(13, HIGH);
    } else if ((char)input != '\r') {
      digitalWrite(13, LOW);
    }
  }
}
